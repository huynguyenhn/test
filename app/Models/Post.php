<?php
namespace App\Models;

class Post extends BaseModel
{
	public function __construct()
	{
		parent::__construct();
	}

	protected $fillable = [
		'title',
		'content',
		'active',
		'user_id',
	];

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}