<?php
namespace App\Models;

use Hash;

class User extends BaseModel
{
	public $fillable = [
        'name',
        'email',
        'password',
        'is_admin'
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function posts()
    {
    	return $this->hasMany(Post::class);
    }

    public function setPasswordAttribute($value)
    {
    	if (!empty($value)) {
    		$this->attributes['password'] = Hash::make($value);
    	}
    }

    public function isAdmin()
    {
    	return $this->is_admin;
    }
}